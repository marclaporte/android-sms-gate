package com.github.axet.smsgate.widgets;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ViewPagerDisabled extends ViewPager {
    public boolean enabled = true;

    public ViewPagerDisabled(Context context) {
        super(context);
    }

    public ViewPagerDisabled(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (!enabled)
            return false;
        return super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!enabled)
            return false;
        return super.onInterceptTouchEvent(ev);
    }
}
