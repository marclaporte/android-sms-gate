package com.github.axet.smsgate.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.services.FirebaseService;
import com.github.axet.smsgate.widgets.MultiImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ShareDialogFragment extends DialogFragment {
    View v;

    MultiImageView filepreview;
    LinearLayout list;

    String title;

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Activity a = getActivity();
        if (a instanceof DialogInterface.OnDismissListener)
            ((DialogInterface.OnDismissListener) a).onDismiss(dialog);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity())
                .setPositiveButton("Share",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                share();
                                dialog.dismiss();
                                onDismiss(dialog);
                            }
                        }
                )
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                onDismiss(dialog);
                            }
                        }
                )
                .setTitle("Share")
                .setView(createView(LayoutInflater.from(getContext()), null, savedInstanceState));
        final AlertDialog d = b.create();
        return d;
    }

    @Nullable
    @Override
    public View getView() {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return null;
    }

    public static String getMimeType(Context context, Uri uri) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        }
        if (mimeType != null)
            return mimeType;

        ContentResolver cR = context.getContentResolver();
        mimeType = cR.getType(uri);
        if (mimeType != null)
            return mimeType;

        String extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
        if (extension != null) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return null;
    }

    public View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.share_dialog, container, false);

        TextView text = (TextView) v.findViewById(R.id.share_text);
        filepreview = (MultiImageView) v.findViewById(R.id.share_preview);
        list = (LinearLayout) v.findViewById(R.id.list);
        View filepanel = v.findViewById(R.id.share_file_panel);

        title = getArguments().getString("text");
        Object uu = getArguments().get("uri");

        if (uu != null) {
            ArrayList<Bundle> bb = new ArrayList<>();
            for (Uri u : (ArrayList<Uri>) uu) {
                Bundle b = add(u);
                bb.add(b);
            }
            getArguments().putParcelableArrayList("uri", bb);
        }

        if (filepreview.getChildCount() == 0) {
            filepreview.setVisibility(View.GONE);
        }

        if (title != null) {
            text.setText(title);
            getArguments().putString("text", title);
        }

        return v;
    }

    Bundle add(Uri uri) {
        Bundle b = new Bundle();
        b.putParcelable("uri", uri);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.share_file, list, false);
        TextView filename = (TextView) v.findViewById(R.id.share_filename);
        TextView filesize = (TextView) v.findViewById(R.id.share_filesize);

        String mime = getMimeType(getActivity(), uri);
        String name = null;
        Long length = null;

        if (Build.VERSION.SDK_INT >= 16) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    name = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));

                    int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                    if (!cursor.isNull(sizeIndex)) {
                        length = cursor.getLong(sizeIndex);
                    }
                }
            } finally {
                if (cursor != null)
                    cursor.close();
            }
        }

        File file = new File(uri.getPath());
        if (file.exists()) {
            if (length == null)
                length = file.length();
            if (name == null)
                name = file.getName();
        }

        if (title == null)
            title = name;

        filename.setText(name);
        if (length != null) {
            filesize.setText(SMSApplication.formatSize(getContext(), length));
            b.putLong("size", length);
        }
        b.putString("filename", name);

        if (mime == null)
            mime = "application/octet-stream";

        b.putString("mimetype", mime);

        if (mime.startsWith("image/")) {
            filepreview.add(uri);
        }

        list.addView(v);
        return b;
    }

    void share() {
        Bitmap bm = filepreview.getPreview();
        if (bm != null) {
            try {
                ByteArrayOutputStream bo = new ByteArrayOutputStream();
                int max = 100;
                if (bm.getWidth() > max) { // reduce bm to 100px width, keep proportions
                    bm = Bitmap.createScaledBitmap(bm, max, (int) (bm.getHeight() * (max / (float) bm.getWidth())), false);
                }
                if (bm.getHeight() > max) { // cut bottom
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), max);
                }
                bm.compress(Bitmap.CompressFormat.JPEG, 95, bo);
                bo.flush();
                getArguments().putByteArray("preview", bo.toByteArray());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        FirebaseService.intent(getContext(), getArguments());
    }
}
