/* Copyright (c) 2009 Christoph Studer <chstuder@gmail.com>
 * Copyright (c) 2010 Jan Berkel <jan.berkel@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.axet.smsgate.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.util.Log;

import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.SmsStorage;
import com.github.axet.smsgate.providers.SIM;
import com.zegoggles.smssync.activity.SMSGateFragment;
import com.zegoggles.smssync.service.ImapSmsService;

import static com.zegoggles.smssync.App.LOCAL_LOGV;
import static com.zegoggles.smssync.App.TAG;

import java.util.HashMap;

public class IncomingPeekReceiver extends BroadcastReceiver { // sms received notification

    public static void incomingSMS(Context context, long last) {
        SMSGateFragment.checkPermissions(context);
        FirebaseService.incoming(context);
        long sms = ImapSmsService.scheduleIncomingBackup(context);
        FileSmsService.incoming(context, last);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (LOCAL_LOGV) Log.v(TAG, "onReceive(" + context + "," + intent + ")");
        String a = intent.getAction();
        if (a == null)
            return;
        if (a.equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            long last = 0;
            SIM sim = SMSApplication.from(context).getSIM();
            HashMap<String, SmsStorage.Message> map = SmsStorage.getMessagesFromIntent(sim, intent);
            for (String key : map.keySet()) {
                SmsStorage.Message m = map.get(key);
                if (last < m.remote_date)
                    last = m.remote_date;
            }
            if (last == 0)
                last = System.currentTimeMillis() - 1000; // we except message from (now - 1sec)
            incomingSMS(context, last);
        }
    }
}
