package com.github.axet.smsgate.widgets;

import android.content.Context;
import android.util.AttributeSet;

public class NameStoragePreferenceCompat extends NameFormatPreferenceCompat {

    public NameStoragePreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public NameStoragePreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public NameStoragePreferenceCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NameStoragePreferenceCompat(Context context) {
        super(context);
    }

    public void onCreate() {
        super.onCreate();
        if (!StoragePathPreferenceCompat.isVisible(getContext()))
            setVisible(false);
    }

}
